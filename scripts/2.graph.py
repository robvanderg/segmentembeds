import myutils
import json
import ast
import statistics
import os

metrics = {'dependency': '.run/dependency/las', 'feats': '.run/feats/acc', 'lemma':'.run/lemma/acc', 'upos':'.run/upos/acc', 'xpos':'.run/xpos/acc'}

def getSettingScore(dataset, embedding, setting, task):
    scores = []
    for seed in myutils.seeds:
        path = '.'.join(['preds/' + dataset, embedding + '-' + setting, seed, 'eval'])
        if not os.path.isfile(path) or os.stat(path).st_size == 0:
            print('Not found: ' + path)
            scores.append(0.0)
        else:
            data = ast.literal_eval('\n'.join(open(path).readlines()))
            scores.append(data[metrics[task]])
    return sum(scores)/len(scores), statistics.pstdev(scores) #or stdev?

def getScores(embedding, setting, task):
    if 'multi' not in embedding:
        datasets = ['UD_English-EWT']
    else:
        datasets = myutils.datasetsUD
    allScores = []
    allStdevs = []
    for dataset in datasets:
        avgScore, stdDev = getSettingScore(dataset, embedding, setting, task)
        allScores.append(avgScore*100)
        allStdevs.append(stdDev*100)
    return sum(allScores)/len(allScores), sum(allStdevs)/len(allStdevs)
    
for task in ['dependency', 'feats', 'lemma', 'upos', 'xpos']:
    print(task)
    allScores = []
    allStdevs = []
    for setting in myutils.settings:
        if setting == 'seg00':
            continue
        allScores.append([])
        allStdevs.append([])
        for embeddings in myutils.embeds:
            avg, stdDev = getScores(embeddings, setting, task)
            allScores[-1].append(avg)
            allStdevs[-1].append(stdDev)
            print(embeddings + '-' + setting, avg, stdDev)

    embedNames = ['en-uncased', 'en-cased', 'ml-uncased', 'ml-cased']

    if task == 'dependency':
        ranges = [(81,86), (68, 73)]
        pos = 'lower right'
    elif task == 'feats':
        ranges = [(93,97), (79,83)]
        pos = 'lower left' 
    elif task == 'lemma':
        ranges = [(91,94), (70,73)]
        pos = 'lower left'
    elif task == 'upos':
        ranges = [(93, 97), (91, 94)]
        pos = 'lower left'
    elif task == 'xpos':
        ranges = [(92,96), (83,87)]
        pos = 'lower left'
    #ranges = [(0,100), (0,100)]
    myutils.graph(allScores, myutils.names, embedNames, 'ud-' + task + '.pdf', pos, allStdevs, ranges[0], ranges[1])

