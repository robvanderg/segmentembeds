import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('scripts/rob.mplstyle')


settings = ['base',  'seg11', 'segmean', 'segzero', 'segrand-24', 'segrand-513', 'segrand-8446']
settings2 = ['base', 'seg11', 'segmean', 'segzero', 'segrand-24', 'segrand-513', 'segrand-8446', 'seg00']
names = ['original', '1s', 'avg', 'null', 'rand-24', 'rand-513', 'rand-8446']
names2 = ['original', '1s', 'avg', 'null', 'rand-24', 'rand-513', 'rand-8446', '0s']

embeds = ['bert-base-uncased', 'bert-base-cased', 'bert-base-multilingual-uncased', 'bert-base-multilingual-cased']

hrSettings = ['bert-base-multilingual-cased', 'mbert-seg11']
hrSettingsGlue = ['bert-base-multilingual-cased', 'mbert-seg11']

datasetsUD = ['UD_Ancient_Greek-PROIEL', 'UD_Arabic-PADT', 'UD_English-EWT', 'UD_Finnish-TDT', 'UD_Chinese-GSD', 'UD_Hebrew-HTB', 'UD_Korean-GSD', 'UD_Russian-GSD', 'UD_Swedish-Talbanken']

seeds = ['513', '8446', '24', '2300', '209', '109', '123', '1234', '12345', '123456', '31'][:5]

datasetsGlue = ['CoLA', 'SST-2']
datasetsGlue2 = ['MNLI', 'MRPC', 'QNLI', 'QQP' , 'RTE', 'SNLI']

def getTrainTuneDevTest(path):
    train = ''
    dev = ''
    test = ''
    tune = ''
    for conllFile in os.listdir(path):
        if conllFile.endswith('train.conllu'):
            train = path + '/' + conllFile
        if conllFile.endswith('tune.conllu'):
            tune = path + '/' + conllFile
        if conllFile.endswith('dev.conllu'):
            dev = path + '/' + conllFile
        if conllFile.endswith('test.conllu'):
            test = path + '/' + conllFile
    return train, tune, dev, test

def getModel(name):
    modelDir = 'mtp/logs/'
    nameDir = modelDir + name + '/'
    if os.path.isdir(nameDir):
        for modelDir in reversed(os.listdir(nameDir)):
            modelPath = nameDir + modelDir + '/model.tar.gz'
            if os.path.isfile(modelPath):
                return modelPath
    return ''

def createParamsConfig(embeddings, setting):
    path = 'configs/' + embeddings + '-' + setting + '.json'
    if not os.path.isfile(path):
        embedsPath = '../data/' + embeddings + '-' + setting
        cmd = 'sed "s;bert-base-multilingual-cased;' + embedsPath + ';g" mtp/configs/params.json > ' + path
        os.system(cmd)
    return path


def graph(data, colorNames, partNames, outName, legend_loc='upper left', stddevs=None, ylim1=None, ylim2=None, double=False):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8,5), dpi=300)

    colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
    colors = colors + colors

    for rowIdx, row in enumerate(data):
        width = 1/(len(data)+1)
        if len(data)%2 == 0:
            dist = rowIdx - (len(data)+1/2)
        else:
            dist = .5 + rowIdx - (len(data)/2)
        x = [i+(dist*width) for i in range(2)]
        if stddevs != None:
            ax1.bar(x,row[:2], width, color=colors[rowIdx], label=colorNames[rowIdx], yerr=stddevs[rowIdx][:2])
            ax2.bar(x,row[2:], width, color=colors[rowIdx], label=colorNames[rowIdx], yerr=stddevs[rowIdx][2:])
        else:
            ax1.bar(x,row[:2], width, color=colors[rowIdx], label=colorNames[rowIdx])
            ax2.bar(x,row[2:], width, color=colors[rowIdx], label=colorNames[rowIdx])

    ax1.set_xticks(range(2))
    ax1.set_xticklabels(partNames[:2], rotation=45, ha='right')
    ax2.set_xticks(range(2))
    ax2.set_xticklabels(partNames[2:], rotation=45, ha='right')
        

    if len(data) == 8:
        ax1.set_xticks([-.5,.5])
        ax1.set_xticklabels(partNames[:2], rotation=45, ha='right')
        ax2.set_xticks([-.5,.5])
        ax2.set_xticklabels(partNames[2:], rotation=45, ha='right')
        ax1.set_xlim(-1.1, .9)
        ax2.set_xlim(-1.1, .9)
    else:
        ax1.set_xlim(-.5, 2-.5)
        ax2.set_xlim(-.5, 2-.5)

    if ylim1 != None:
        ax1.set_ylim(ylim1)
    if ylim2 != None:
        ax2.set_ylim(ylim2)
    col = 1
    if double:
        col = 2
    leg = ax1.legend(ncol=4, bbox_to_anchor=(0.0, 1.0), loc='lower left')
    leg.get_frame().set_linewidth(1.5)
    fig.savefig(outName, bbox_inches='tight')


def graph2(data, colorNames, partNames, outName, legend_loc='upper left', stddevs=None, ylim=None):
    fig, ax = plt.subplots(figsize=(4,5), dpi=300)

    #fig, ax = plt.subplots(figsize=(8,5), dpi=300)
    colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
    colors = colors + colors

    for rowIdx, row in enumerate(data):
        width = 1/(len(data)+1)
        if len(data)%2 == 0:
            dist = rowIdx - (len(data)+1/2)
        else:
            dist = .5 + rowIdx - (len(data)/2)
        x = [i+(dist*width) for i in range(2)]
        if stddevs != None:
            ax.bar(x,row[:2], width, color=colors[rowIdx], label=colorNames[rowIdx], yerr=stddevs[rowIdx][:2])
        else:
            ax.bar(x,row[:2], width, color=colors[rowIdx], label=colorNames[rowIdx])

    ax.set_xticks(range(2))
    ax.set_xticklabels(['ml-uncased', 'ml-cased'], rotation=45, ha='right')

    ax.set_xlim(-.5, 2-.5)
    if ylim != None:
        ax.set_ylim(ylim)
    leg = ax.legend(ncol=4, bbox_to_anchor=(-0.5, 1.0), loc='lower left')
    leg.get_frame().set_linewidth(1.5)
    fig.savefig(outName, bbox_inches='tight')


