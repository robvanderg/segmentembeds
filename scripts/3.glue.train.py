from allennlp.common import Params
import os
import myutils

def getSize(path):
    return len(open(path).readlines())

def createDatasetConfig(dataset, highResource):
    config = {dataset:{}}
    config[dataset]['train_data_path'] = '../data/glue/' + dataset + '.train'
    config[dataset]['validation_data_path'] = '../data/glue/' + dataset + '.tune'
    config[dataset]['tasks'] = {dataset:{}}
    config[dataset]['tasks'][dataset]['task_type'] = 'classification'
    if dataset in myutils.datasetsGlue:
        config[dataset]['sent_idxs'] = [0]
        config[dataset]['tasks'][dataset]['column_idx'] = 1
    else:
        config[dataset]['sent_idxs'] = [0,1]
        config[dataset]['tasks'][dataset]['column_idx'] = 2

    if not highResource:
        config[dataset]['max_sents'] = int(1000)#getSize('data/glue/' + dataset + '.train')/10)
        Params(config).to_file('configs/' + dataset + '.json')
    else:
        Params(config).to_file('configs/' + dataset + '-hr.json')

for dataset in myutils.datasetsGlue + myutils.datasetsGlue2:
    createDatasetConfig(dataset, False)
    createDatasetConfig(dataset, True)

for embeddings in myutils.embeds:
    for setting in myutils.settings:
        paramPath = myutils.createParamsConfig(embeddings, setting)
        for dataset in myutils.datasetsGlue + myutils.datasetsGlue2:
            if dataset in myutils.datasetsGlue and setting == 'seg00':
                continue
            for seed in myutils.seeds:
                name = dataset + '.' + embeddings + '-' + setting + '.' + seed
                if myutils.getModel(name) == '':
                    cmd = 'cd mtp && python3 train.py --dataset_config ../configs/' + dataset + '.json --parameters_config ../' + paramPath + ' --name ' + name + ' --seed ' + seed + ' && cd ../'
                    print(cmd)

                #if setting in myutils.hrSettingsGlue:
                #    name = setting + '.' + dataset + '-hr.' + seed
                #    if myutils.getModel(name) == '':
                #        cmd = 'cd mtp && python3 train.py --dataset_config ../configs/' + dataset + '-hr.json --parameters_config ../' + paramPath + ' --name ' + name + ' --seed ' + seed + ' && cd ../'
                #        print(cmd)
                


