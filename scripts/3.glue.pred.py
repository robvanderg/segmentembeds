from allennlp.common import Params
import os
import myutils

def pred(setting, dataset, seed, highResource):
    name = dataset + '.' + setting + '.' + seed
    if highResource:
        name = setting + '.' + dataset + '-hr.' + seed
    modelPath = myutils.getModel(name)
    if modelPath == '':
        print('Not found: ' + name)
        return
    dev  = 'data/glue/' + dataset + '.dev'
    out = '../preds/' + name
    if not os.path.isfile(out[3:]):
        cmd = 'cd mtp && python3 predict.py ' + modelPath[4:] + ' ../' + dev + ' ' + out + ' > ' + out + '.eval && cd ../'
        print(cmd)


for embeddings in myutils.embeds:
    for setting in myutils.settings:
        for dataset in myutils.datasetsGlue + myutils.datasetsGlue2:
            for seed in myutils.seeds:
                pred(embeddings + '-' + setting, dataset, seed, False)



