from allennlp.common import Params
import os
import myutils

def pred(setting, dataset, seed, highResource):
    name = dataset + '.' + setting + '.' + seed
    if highResource:
        name += '-hr'
    modelPath = myutils.getModel(name)
    if modelPath == '':
        print('Not found: ' + name)
        return
    _,_,dev,_ = myutils.getTrainTuneDevTest('data/ud-treebanks-v2.8.noEUD/' + dataset)
    out = '../preds/' + name 
    if not os.path.isfile(out[3:]):
        cmd = 'cd mtp && python3 predict.py ' + modelPath[4:] + ' ../' + dev + ' ' + out + ' > ' + out + '.eval && cd ../' 
        print(cmd)

for embeddings in myutils.embeds:
    for setting in myutils.settings:
        if setting == 'seg00':
            continue
        for dataset in myutils.datasetsUD:
            for seed in myutils.seeds:
                pred(embeddings + '-' + setting, dataset, seed, False)

                if 'multilingual' in embeddings:
                    pred(embeddings + '-' + setting, dataset, seed, True)


                

