mkdir -p data
mkdir -p config
mkdir -p preds


git clone https://bitbucket.org/ahmetustunn/mtp.git
cd mtp
git reset --hard d5e188792550c352c28ed1a78e0a083e8a3e1540
cd ..

wget https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3687/ud-treebanks-v2.8.tgz
tar -zxvf ud-treebanks-v2.8.tgz
mv ud-treebanks-v2.8 data
cp -r data/ud-treebanks-v2.8 data/ud-treebanks-v2.8.noEUD
cd mtp
python3 scripts/misc/cleanconl.py ../data/ud-treebanks-v2.8.noEUD/*/*conllu
cd ../

python3 scripts/0.prep.ud.resplit.py data/ud-treebanks-v2.8/
python3 scripts/0.prep.ud.resplit.py data/ud-treebanks-v2.8.noEUD/

python3 scripts/0.prep.glue.py
python3 scripts/0.prep.glue.resplit.py

python3 scripts/0.genMBerts.py

