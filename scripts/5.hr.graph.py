import myutils
import json
import ast
import statistics
import os

def getSettingScore(dataset, embedding, setting):
    scores = []
    for seed in myutils.seeds:
        path = '.'.join(['preds/' + dataset, embedding + '-' + setting, seed + '-hr', 'eval'])
        if not os.path.isfile(path) or os.stat(path).st_size == 0:
            print('Not found: ' + path)
            scores.append(0.0)
        else:
            data = ast.literal_eval('\n'.join(open(path).readlines()))
            scores.append(data['.run/dependency/las'])
    return sum(scores)/len(scores), statistics.pstdev(scores) #or stdev?

def getScores(embedding, setting):
    if 'multi' not in embeddings:
        datasets = ['UD_English-EWT']
    else:
        datasets = myutils.datasetsUD
    allScores = []
    allStdevs = []
    for dataset in datasets:
        avgScore, stdDev = getSettingScore(dataset, embedding, setting)
        allScores.append(avgScore*100)
        allStdevs.append(stdDev*100)
    return sum(allScores)/len(allScores), sum(allStdevs)/len(allStdevs)
        
allScores = []
allStdevs = []
for setting in myutils.settings:
    allScores.append([])
    allStdevs.append([])
    for embeddings in myutils.embeds[-2:]:
        avg, stdDev = getScores(embeddings, setting)
        allScores[-1].append(avg)
        allStdevs[-1].append(stdDev)
        print(embeddings + '-' + setting, avg, stdDev)

embedNames = ['en-uncased', 'en-cased', 'ml-uncased', 'ml-cased']

print(allScores)
myutils.graph2(allScores, myutils.names, embedNames, 'ud-hr.pdf', 'lower left', allStdevs, (83, 86))

