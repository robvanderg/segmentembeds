import myutils
import json
import ast

def getScores(dataset, setting, highResource):
    for seed in myutils.seeds:
        model = myutils.getModel(setting + '.' + dataset + '.' + seed)
        if highResource:
            model = myutils.getModel(setting + '.' + dataset + '-hr.' + seed)
        scores = []
        for i in range(20):
            path = model.replace('model.tar.gz', 'metrics_epoch_' + str(i) + '.json')
            data = json.load(open(path))
            scores.append(data['best_validation_.run/.sum'])
        tuneScore = sum(scores)/len(scores)

    evalPath = 'preds/' + setting + '.' + dataset + '.' + seed + '.eval'
    if highResource:
        evalPath = 'preds/' + setting + '.' + dataset + '-hr.' + seed + '.eval'
    devScore = ast.literal_eval('\n'.join(open(evalPath).readlines()))['.run/.sum']

    return tuneScore, devScore
        
def avgStr(vals):
    return '{:.2f}'.format(100*sum(vals)/len(vals))

def getRow(setting, highResource):
    tuneScores = []
    devScores = []
    #for dataset in myutils.datasetsGlue:
    for dataset in myutils.datasetsGlue2:
        tuneScore, devScore = getScores(dataset, setting, highResource)
        tuneScores.append(tuneScore)
        devScores.append(devScore)
    if highResource:
        print(setting + '-hr & ' + avgStr(tuneScores) + ' & ' + avgStr(devScores) + ' \\\\') 
    else:
        print(setting + ' & ' + avgStr(tuneScores) + ' & ' + avgStr(devScores) + ' \\\\') 


for hrSetting in myutils.hrSettingsGlue:
    getRow(hrSetting, True)
for setting in myutils.settings:
    getRow(setting, False)


