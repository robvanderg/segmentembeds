import myutils
import json
import ast
import statistics
import os

def getSettingScore(dataset, embedding, setting):
    scores = []
    for seed in myutils.seeds:
        path = '.'.join(['preds/' + dataset, embedding + '-' + setting, seed, 'eval'])
        if not os.path.isfile(path) or os.stat(path).st_size == 0:
            print('Not found: ' + path)
            scores.append(0.0)
        else:
            data = ast.literal_eval('\n'.join(open(path).readlines()))
            scores.append(data['.run/dependency/las'])
    return sum(scores)/len(scores), statistics.pstdev(scores) #or stdev?

def getScores(embedding, setting, dataset):
    #if 'multi' not in embeddings:
    #    datasets = ['UD_English-EWT']
    #else:
    #    datasets = myutils.datasetsUD
    allScores = []
    allStdevs = []
    avgScore, stdDev = getSettingScore(dataset, embedding, setting)
    allScores.append(avgScore*100)
    allStdevs.append(stdDev*100)
    return sum(allScores)/len(allScores), sum(allStdevs)/len(allStdevs)
    
for dataset in myutils.datasetsUD:
    allScores = []
    allStdevs = []
    for setting in myutils.settings:
        allScores.append([])
        allStdevs.append([])
        for embeddings in myutils.embeds:
            avg, stdDev = getScores(embeddings, setting, dataset)
            allScores[-1].append(avg)
            allStdevs[-1].append(stdDev)
            print(embeddings + '-' + setting, avg, stdDev)

    embedNames = ['en-uncased', 'en-cased', 'ml-uncased', 'ml-cased']

    if dataset =='UD_Ancient_Greek-PROIEL':
        ranges = [(37,55), (51, 69)]
    elif dataset == 'UD_Arabic-PADT':
        ranges = [(55,65), (65,75)]
    elif dataset == 'UD_English-EWT':
        ranges = [(82,86), (82,86)]
    elif dataset == 'UD_Finnish-TDT':
        ranges = [(42,48), (70,76)]
    elif dataset == 'UD_Chinese-GSD':
        ranges = [(18,28), (64,74)]
    elif dataset == 'UD_Hebrew-HTB':
        ranges = [(35,40), (70,75)]
    elif dataset == 'UD_Korean-GSD':
        ranges = [(26,38), (62,74)]
    elif dataset == 'UD_Russian-GSD':
        ranges = [(50,58), (73,81)]
    elif dataset == 'UD_Swedish-Talbanken':
        ranges = [(37,42), (65,70)]

    myutils.graph(allScores, myutils.names, embedNames, dataset + '.pdf', 'lower right', allStdevs, ranges[0], ranges[1])


