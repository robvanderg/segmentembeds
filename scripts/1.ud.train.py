from allennlp.common import Params
import os
import myutils

def getSize(path):
    counter = 0
    for line in open(path):
        if len(line) < 3:
            counter += 1
    return counter

def createDatasetConfig(dataset, highResource):
    dataParams = Params.from_file('mtp/configs/ewt.json')
    train, tune, _, _ = myutils.getTrainTuneDevTest('data/ud-treebanks-v2.8.noEUD/' + dataset)
    dataParams['UD'] = dataParams['UD_EWT'].as_dict()
    del dataParams['UD_EWT']
    dataParams['UD']['train_data_path'] = '../' + train
    dataParams['UD']['validation_data_path'] = '../' + tune
    if not highResource:
        dataParams['UD']['max_sents'] = int(getSize(train)/10)
        dataParams.to_file('configs/' + dataset + '.json')
    else:
        dataParams.to_file('configs/' + dataset + '-hr.json')

for dataset in myutils.datasetsUD:
    createDatasetConfig(dataset, False)
    createDatasetConfig(dataset, True)

for embeddings in myutils.embeds:
    for setting in myutils.settings:
        if setting == 'seg00':
            continue
        paramPath = myutils.createParamsConfig(embeddings, setting)
        for dataset in myutils.datasetsUD:
            if 'multilingual' not in embeddings and dataset != 'UD_English-EWT':
                continue
            for seed in myutils.seeds:
                name = dataset + '.' + embeddings + '-' + setting + '.' + seed
                if myutils.getModel(name) == '':
                    
                    cmd = 'cd mtp && python3 train.py --dataset_config ../configs/' + dataset + '.json --parameters_config ../' + paramPath + ' --name ' + name + ' --seed ' + seed + ' && cd ../'
                    print(cmd)
                if myutils.getModel(name+'-hr') == '':
                    cmd = 'cd mtp && python3 train.py --dataset_config ../configs/' + dataset + '-hr.json --parameters_config ../' + paramPath + ' --name ' + name + '-hr --seed ' + seed + ' && cd ../'
                    print(cmd)

