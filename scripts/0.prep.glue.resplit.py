import myutils
import os
import sys

def getSize(path):
    if os.path.isfile(path):
        return len(open(path).readlines())
    return 0

def resize(origPath, lastN, newPath):
    data = open(origPath).readlines()
    newFile = open(newPath, 'w')
    for sent in data[-lastN:]:
        newFile.write(''.join(sent))
    newFile.close()
    newOrigFile = open(origPath, 'w')
    for sent in data[:-lastN]:
        newOrigFile.write(''.join(sent))
    newOrigFile.close()
    

for glueTask in myutils.datasetsGlue + myutils.datasetsGlue2:
    trainPath = 'data/glue/'  + glueTask + '.train' 
    trainSize = getSize(trainPath)
    tunePath = 'data/glue/'  + glueTask + '.tune'  
    resize(trainPath, 500, tunePath) 

