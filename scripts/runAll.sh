./scripts/0.prep.sh

python3 scripts/0.genMBerts.py

python3 scripts/1.ud.train.py > 1.ud.train.sh
chmod +x 1.ud.train.sh
./1.ud.train.sh

python3 scripts/1.pred.py > 1.ud.pred.sh
chmod +x 1.ud.pred.sh
./1.ud.pred.sh

python3 scripts/3.glue.train.py > 3.glue.train.sh
chmod +x 3.glue.train.sh
./3.glue.train.sh

python3 scripts/3.glue.pred.py > 3.glue.pred.sh
chmod +x 3.glue.pred.sh
./3.glue.pred.sh

