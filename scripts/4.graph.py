import myutils
import json
import ast
import statistics
import os

def getSettingScore(dataset, embedding, setting):
    scores = []
    for seed in myutils.seeds[1:]:
        path = '.'.join(['preds/' + dataset, embedding + '-' + setting, seed, 'eval'])
        if not os.path.isfile(path) or os.stat(path).st_size == 0:
            print('Not found: ' + path)
            #scores.append(0.0)
        else:
            data = ast.literal_eval('\n'.join(open(path).readlines()))
            scores.append(data['.run/.sum'])
    return sum(scores)/len(scores), statistics.pstdev(scores) #or stdev?

def getScores(embedding, setting, datasets):
    allScores = []
    allStdevs = []
    for dataset in datasets:
        avgScore, stdDev = getSettingScore(dataset, embedding, setting)
        allScores.append(avgScore)
        allStdevs.append(stdDev)
    return sum(allScores)/len(allScores), sum(allStdevs)/len(allStdevs)
        
for datasets in [myutils.datasetsGlue, myutils.datasetsGlue2]:
    allScores = []
    allStdevs = []
    for setting in myutils.settings2:
        if setting == 'seg00' and 'CoLA' in datasets:
            continue
        allScores.append([])
        allStdevs.append([])
        for embeddings in myutils.embeds:
            avg, stdDev = getScores(embeddings, setting, datasets)
            allScores[-1].append(avg*100)
            allStdevs[-1].append(stdDev*100)
            print(embeddings + '-' + setting, avg, stdDev)

    embedNames = ['en-uncased', 'en-cased', 'ml-uncased', 'ml-cased']
    out = 'glue.single.pdf' if 'CoLA'  in datasets else 'glue.pair.pdf' 
    names = myutils.names if 'CoLA'  in datasets else myutils.names2 
    if 'CoLA' in datasets:
        myutils.graph(allScores, names, embedNames, out, 'upper right', allStdevs, (79, 84), (72, 77))
    else:
        myutils.graph(allScores, names, embedNames, out, 'upper right', allStdevs, (70, 79), (70, 79), True)

