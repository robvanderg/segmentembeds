import myutils
import json
import ast

def getScores(dataset, setting, highResource):
    for seed in myutils.seeds:
        model = myutils.getModel(setting + '.' + dataset + '.' + seed)
        if highResource:
            model = myutils.getModel(setting + '.' + dataset + '-hr.' + seed)
        scores = []
        for i in range(20):
            path = model.replace('model.tar.gz', 'metrics_epoch_' + str(i) + '.json')
            data = json.load(open(path))
            scores.append(data['validation_.run/dependency/las'])
        tuneScore = sum(scores)/len(scores)

    path = 'preds/' + setting + '.' + dataset + '.' + seed + '.eval'
    if highResource:
        path = 'preds/' + setting + '.' + dataset + '-hr.' + seed + '.eval'
    data = ast.literal_eval('\n'.join(open(path).readlines()))
    devScore = data['.run/dependency/las']

    return tuneScore, devScore
        
def avgStr(vals):
    return '{:.2f}'.format(100*sum(vals)/len(vals))

def getRow(setting, highResource):
    tuneScores = []
    devScores = []
    for dataset in myutils.datasets:
        tuneScore, devScore = getScores(dataset, setting, highResource)
        tuneScores.append(tuneScore)
        devScores.append(devScore)
    if highResource:
        print(setting + '-hr & ' + avgStr(tuneScores) + ' & ' + avgStr(devScores) + ' \\\\') 
    else:
        print(setting + ' & ' + avgStr(tuneScores) + ' & ' + avgStr(devScores) + ' \\\\') 


#for hrSetting in myutils.hrSettings:
#    getRow(hrSetting, True)
for embeds in myutils.embeds:
    for setting in myutils.settings:
        print(embeds + '-' + setting)
        #getRow(setting, False)


