import os
import ast
import myutils
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('scripts/rob.mplstyle')

def getSettingScore(dataset, embedding, setting, epoch):
    scores = []
    for seed in myutils.seeds:
        name = dataset + '.' + embedding + '-' + setting + '.' + seed + '-hr'
        modelPath = myutils.getModel(name)
        resultsPath = modelPath.replace('model.tar.gz', 'metrics_epoch_' + str(epoch) + '.json')
        if modelPath == '':
            print('Model not found: ' + name)
            scores.append(0.0)
        else:
            data = ast.literal_eval('\n'.join(open(resultsPath).readlines()))
            scores.append(data['best_validation_.run/dependency/las'])
    return sum(scores)/len(scores)

def getScores(embedding, setting):
    datasets = myutils.datasetsUD
    epochScores = []
    for epoch in range(10):
        allScores = []
        for dataset in datasets:
            avgScore = getSettingScore(dataset, embedding, setting, epoch)
            allScores.append(avgScore*100)
        epochScores.append(sum(allScores)/len(allScores))
    return epochScores


fig, ax = plt.subplots(figsize=(8,5), dpi=300)
for setting, name in zip(myutils.settings, myutils.names):
    scores = getScores('bert-base-multilingual-cased', setting)
    ax.plot(range(len(scores)), scores, label=name, alpha=0.8)

ax.set_xticks(range(10))
ax.set_xticklabels(range(1,11))
ax.set_xlabel('Epoch')
ax.set_ylabel("LAS")
leg = ax.legend()
leg.get_frame().set_linewidth(1.5)
fig.savefig('epochs.pdf', bbox_inches='tight')



