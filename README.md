# Frustratingly Easy Performance Improvements for Cross-lingual Transfer: A Tale on BERT and Segment Embeddings

This is the repository with the code for "Frustratingly Easy Performance Improvements for Cross-lingual Transfer: A Tale on BERT and Segment Embeddings"

## Contents

* configs: contains all configuration files necessary to run MaChAmp for all our setups,
* preds: contains all scores of our evaluation runs
* scripts: contains all scripts do download the necessary data and MaChAmp, generate the config files, run the experiments, and generate the tables/figures in the paper
* preds.tar.gz: contains also the conllu output of the parser for all runs
* epochs.tar.gz: contains the scores for each epoch for the high-resource word-level setup, used for Figure 6 in the paper

## Reproducability
All scripts to reproduce our results can be found in `scripts/runAll.sh`. We use the approach described in detail [here](https://robvanderg.github.io/blog/repro.htm) for reproducability.


